//
//  ViewController.swift
//  Calculator
//
//  Created by soroush amini araste on 5/9/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit


enum Operation: String {
    case Add
    case Subtract
    case Divide
    case Multiply
    case Percentage
    case SignChange
    case Null
    case Equal
}

var _lightOrange: UIColor = UIColor(named: "_LightOrange")!

class ViewController: UIViewController {
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var pluseBtnOutlet: CustomCalculatorButton!
    @IBOutlet weak var subtractBtnOutlet: CustomCalculatorButton!
    @IBOutlet weak var divideBtnOutlet: CustomCalculatorButton!
    @IBOutlet weak var multiplyBtnOutlet: CustomCalculatorButton!
    
    var runningNumber: Double = 0
    var leftValue: Double = 0
    var rightValue: Double = 0
    var finalResult: Double = 0
    var currentOperation: Operation    = .Null
    var hasDecimal: Bool = false
    var counter: Double = 0
    var decimalFlag: Int = 0
    var decimaledResult: Double = 0.0
    var temp1: Decimal = 0
    var temp2: Double = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resultLabel.text = "\(Int(runningNumber))"
        
    }
    func changeSelectedOperation(operation: Operation) {
        
        switch operation {
        case .Add:
            pluseBtnOutlet.isSelected = true
            pluseBtnOutlet.titleColor(for: .selected)
            pluseBtnOutlet.backgroundColor = .white
            
            subtractBtnOutlet.isSelected = false
            multiplyBtnOutlet.isSelected = false
            divideBtnOutlet.isSelected = false
            
            subtractBtnOutlet.backgroundColor = _lightOrange
            multiplyBtnOutlet.backgroundColor = _lightOrange
            divideBtnOutlet.backgroundColor = _lightOrange
            
        case .Subtract:
            subtractBtnOutlet.isSelected = true
            subtractBtnOutlet.titleColor(for: .selected)
            subtractBtnOutlet.backgroundColor = .white
            
            multiplyBtnOutlet.isSelected = false
            divideBtnOutlet.isSelected = false
            pluseBtnOutlet.isSelected = false
            
            pluseBtnOutlet.backgroundColor = _lightOrange
            multiplyBtnOutlet.backgroundColor = _lightOrange
            divideBtnOutlet.backgroundColor = _lightOrange
            
        case .Multiply:
            multiplyBtnOutlet.isSelected = true
            multiplyBtnOutlet.titleColor(for: .selected)
            multiplyBtnOutlet.backgroundColor = .white
            
            subtractBtnOutlet.isSelected = false
            divideBtnOutlet.isSelected = false
            pluseBtnOutlet.isSelected = false
            
            pluseBtnOutlet.backgroundColor = _lightOrange
            subtractBtnOutlet.backgroundColor = _lightOrange
            divideBtnOutlet.backgroundColor = _lightOrange
            
        case .Divide:
            divideBtnOutlet.isSelected = true
            divideBtnOutlet.titleColor(for: .selected)
            divideBtnOutlet.backgroundColor = .white
            
            multiplyBtnOutlet.isSelected = false
            subtractBtnOutlet.isSelected = false
            pluseBtnOutlet.isSelected = false
            
            pluseBtnOutlet.backgroundColor = _lightOrange
            multiplyBtnOutlet.backgroundColor = _lightOrange
            subtractBtnOutlet.backgroundColor = _lightOrange
        default:
            pluseBtnOutlet.isSelected = false
            subtractBtnOutlet.isSelected = false
            divideBtnOutlet.isSelected = false
            multiplyBtnOutlet.isSelected = false
            
            pluseBtnOutlet.backgroundColor = _lightOrange
            multiplyBtnOutlet.backgroundColor = _lightOrange
            subtractBtnOutlet.backgroundColor = _lightOrange
            divideBtnOutlet.backgroundColor = _lightOrange
            
            pluseBtnOutlet.titleColor(for: .normal)
            subtractBtnOutlet.titleColor(for: .normal)
            multiplyBtnOutlet.titleColor(for: .normal)
            divideBtnOutlet.titleColor(for: .normal)
            
        }
    }
    
    @IBAction func numberPressed(_ sender: CustomCalculatorButton) {
        //print("counter -> \(counter)")
        if runningNumber <= 99999999 && finalResult <= 99999999 {
            if hasDecimal == false {
                if counter < 1 {
                    runningNumber = Double(sender.tag)
                } else {
                    let temp = Double(runningNumber) * 10
                    runningNumber = temp + Double(sender.tag)
                }
                resultLabel.text = "\(Int(runningNumber))"
                counter += 1
            } else if hasDecimal == true {
                if counter < 1 {
                    runningNumber = Double(sender.tag)
                    print("here : \(String(runningNumber).count)")
                    print(runningNumber)
                } else {
                    let temp = Double(runningNumber) * 10
                    runningNumber = temp + Double(sender.tag)
                    
                    //resultLabel.text = "\(Int(runningNumber))"
                    counter += 1
                    let countOfrunningNumber = String(runningNumber).count
                    print("running number -> \(runningNumber)")
                    print("count of running number -> \(countOfrunningNumber)")
                    let diffrence = countOfrunningNumber - decimalFlag
                    print("diffrence -> \(diffrence)")
                    decimaledResult = 0.0
                    
                    decimaledResult = runningNumber / pow(10, Double(diffrence))
                    print("This is decimaled Result -> \(decimaledResult)")
                    
                    if runningNumber.remainder(dividingBy: 10) == 0 {
                        resultLabel.text = "\(decimaledResult)" + "0"
                    } else {
                        resultLabel.text = "\(decimaledResult)"
                    }
                    
                    //BUG: when we pressed zere after decimal it doesn't show but it's added to running number
                    
                    
                    
                }
            }
        } else {
            print("hello")
        }
    }
    @IBAction func allClearPressed(_ sender: CustomCalculatorButton) {
        runningNumber = 0
        leftValue = 0
        rightValue = 0
        finalResult = 0
        currentOperation = .Null
        resultLabel.text = "\(0)"
        counter = 0
        hasDecimal = false
        decimalFlag = 0
        decimaledResult = 0
        changeSelectedOperation(operation: .Null)
    }
    @IBAction func signButtonPressed(_ sender: CustomCalculatorButton) {
        changeSelectedOperation(operation: .Null)
        if runningNumber != 0 {
            runningNumber = runningNumber * -1.0
            if runningNumber.truncatingRemainder(dividingBy: 1) == 0 {
                resultLabel.text = "\(Int(runningNumber))"
            } else {
                resultLabel.text = "\(runningNumber)"
            }
            
        } else if runningNumber == 0 {
            //print(runningNumber)
            if finalResult > 99_999_999 {
                resultLabel.text = "ERROR"
            } else {
                if finalResult.truncatingRemainder(dividingBy: 1) == 0 {
                    finalResult = finalResult * -1
                    leftValue = leftValue * -1
                    resultLabel.text = "\(Int(finalResult))"
                } else {
                    finalResult = finalResult * -1
                    leftValue = leftValue * -1
                    resultLabel.text = "\(finalResult)"
                }
            }
        }
        
    }
    @IBAction func percentageButtonPressed(_ sender: CustomCalculatorButton) {
        changeSelectedOperation(operation: .Null)
        if runningNumber < 99_999_999 && finalResult < 99_999_999 {
            if runningNumber != 0 {
                runningNumber = runningNumber / 100
                if runningNumber.truncatingRemainder(dividingBy: 1) == 0 {
                    resultLabel.text = "\(Int(runningNumber))"
                } else {
                    resultLabel.text = "\(runningNumber)"
                }
            } else {
                finalResult = finalResult / 100
                leftValue = finalResult
                if finalResult.truncatingRemainder(dividingBy: 1) == 0 {
                    resultLabel.text = "\(Int(finalResult))"
                } else {
                    resultLabel.text = "\(finalResult)"
                }
            }
        } else {
            resultLabel.text = "ERROR"
        }
        
    }
    @IBAction func devideButtonPressed(_ sender: CustomCalculatorButton) {
        operation(operation: .Divide)
        changeSelectedOperation(operation: .Divide)
    }
    @IBAction func multiplyButtonPressed(_ sender: CustomCalculatorButton) {
        operation(operation: .Multiply)
        changeSelectedOperation(operation: .Multiply)
    }
    @IBAction func subtractButtonPressed(_ sender: CustomCalculatorButton) {
        operation(operation: .Subtract)
        changeSelectedOperation(operation: .Subtract)
    }
    @IBAction func plusButtonPressed(_ sender: CustomCalculatorButton) {
        operation(operation: .Add)
        changeSelectedOperation(operation: .Add)
    }
    @IBAction func equalButtonPressed(_ sender: CustomCalculatorButton) {
        operation(operation: currentOperation)
        changeSelectedOperation(operation: .Equal)
    }
    @IBAction func dotButtonPressed(_ sender: CustomCalculatorButton) {
        changeSelectedOperation(operation: .Null)
        print(runningNumber)
        hasDecimal = true
        counter = 1
        
        if runningNumber <= 9999999 && finalResult <= 9999999 {
            decimalFlag = String(runningNumber).count
            if runningNumber == 0 {
                decimalFlag -= 1
            }
            print("decimal flag -> \(decimalFlag)")
            resultLabel.text = "\(Int(runningNumber))."
        }
        
    }
    
    func operation(operation: Operation) {
        if hasDecimal == true {
            runningNumber = decimaledResult
            counter = 1
            hasDecimal = false
        }
        if currentOperation != .Null  {
            if runningNumber != 0 {
                rightValue = runningNumber
                runningNumber = 0
                
                if currentOperation == .Add {
                    finalResult = leftValue + rightValue
                } else if currentOperation == .Subtract {
                    finalResult = leftValue - rightValue
                } else if currentOperation == .Multiply {
                    finalResult = leftValue * rightValue
                } else if currentOperation == .Divide {
                    finalResult = leftValue / rightValue
                }
                
                leftValue = finalResult
                
                if finalResult > 99_999_999 {
                    resultLabel.text = "ERROR"
                } else {
                    print("this is left value \(leftValue)")
                    print("this is right value \(rightValue)")
                    print("this is final result \(finalResult)")
                    if finalResult.truncatingRemainder(dividingBy: 1) == 0 {
                        resultLabel.text = "\(Int(finalResult))"
                    } else {
                        resultLabel.text = "\(finalResult)"
                    }
                }
                
            }
            currentOperation = operation
        } else {
            leftValue = runningNumber
            runningNumber = 0
            currentOperation = operation
        }
    }
}


