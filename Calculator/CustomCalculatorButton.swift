//
//  CustomCalculatorButton.swift
//  Calculator
//
//  Created by soroush amini araste on 5/9/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit

@IBDesignable
class CustomCalculatorButton: UIButton {
    
    @IBInspectable var roundButton: Bool = false {
        didSet {
            if roundButton {
                layer.cornerRadius = frame.height / 2
            }
        }
    }
    override func prepareForInterfaceBuilder() {
        if roundButton {
            layer.cornerRadius = frame.height / 2
        }
    }
}
